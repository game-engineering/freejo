extends Node

class_name StateMachine

var current_state : State
var running := false
var last_state : State


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if current_state:
		current_state._process_state(delta)


func next(state: State):
	if current_state:
		current_state._exit_state()
	last_state = current_state
	current_state = state
	current_state._enter_state()
