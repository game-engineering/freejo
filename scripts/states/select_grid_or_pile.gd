extends State

@onready var game : Game = $"../.."
@onready var select_pile = $"../SelectPile"
@onready var select_grid = $"../SelectGrid"
@onready var finished = $"../Finished"
@onready var end_turn = $"../EndTurn"

func _enter_state():
	game.open_pile.ready_for_action = true
	game.open_pile.connect("clicked", _open_pile_clicked)
	game.current_player.grid.set_all_piles_ready_for_action(true, false)
	game.current_player.grid.connect("pile_clicked", _grid_clicked)


func _exit_state():
	game.open_pile.ready_for_action = false
	game.open_pile.disconnect("clicked", _open_pile_clicked)
	game.current_player.grid.set_all_piles_ready_for_action(false)
	game.current_player.grid.disconnect("pile_clicked", _grid_clicked)
	
func _open_pile_clicked(_pile):
	await game.active_card.move_top_card_to_pile(game.open_pile)
	get_parent().next(select_grid)

	
func _grid_clicked(pile: Pile):
	var card = pile.get_top_card()
	if not card.face_visible:
		await card.turn_card()
	await pile.move_top_card_to_pile(game.open_pile)
	# game.open_pile.add_card(pile.take_card())
	await game.active_card.move_top_card_to_pile(pile)
	get_parent().next(end_turn)
