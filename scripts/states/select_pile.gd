extends State

@onready var game : Game = $"../.."
@onready var select_grid_or_pile = $"../SelectGridOrPile"
@onready var select_grid = $"../SelectGrid"

func _enter_state():
	game.open_pile.ready_for_action = true
	game.open_pile.connect("clicked", _open_pile_clicked)
	game.closed_pile.ready_for_action = true
	game.closed_pile.connect("clicked", _closed_pile_clicked)

func _exit_state():
	game.open_pile.ready_for_action = false
	game.open_pile.disconnect("clicked", _open_pile_clicked)
	game.closed_pile.ready_for_action = false
	game.closed_pile.disconnect("clicked", _closed_pile_clicked)
	
func _open_pile_clicked(_pile):
	await game.open_pile.move_top_card_to_pile(game.active_card)
	get_parent().next(select_grid)

func _closed_pile_clicked(_pile):
	await game.closed_pile.get_top_card().turn_card()
	await game.closed_pile.move_top_card_to_pile(game.active_card)
	get_parent().next(select_grid_or_pile)
