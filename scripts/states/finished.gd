extends State

@onready var game : Game = $"../.."
@onready var setup_round = $"../SetupRound"
@onready var next_round = $NextRound
@onready var clear_scores = $ClearScores

func _ready():
	next_round.hide()
	clear_scores.hide()
	
func _enter_state():
	# Penalty if the finishing player does not win this round
	if game.current_player == game.player_1:
		if game.player_1.current_score >= game.player_2.current_score:
			game.player_1.current_score *= 2
	else:
		if game.player_2.current_score >= game.player_1.current_score:
			game.player_2.current_score *= 2
	
	game.player_1.add_score(game.player_1.current_score)
	game.player_2.add_score(game.player_2.current_score)
	next_round.show()
	clear_scores.show()
	game.arrow.hide()

func _exit_state():
	next_round.hide()
	clear_scores.hide()

func _on_next_round_pressed():
	get_parent().next(setup_round)


func _on_clear_scores_pressed():
	game.player_1.clear_scores()
	game.player_2.clear_scores()
