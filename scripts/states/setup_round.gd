extends State

const CardScene : PackedScene = preload("res://scenes/card.tscn")

@onready var game : Game = $"../.."
@onready var select_pile = $"../SelectPile"
@onready var open_2_cards = $"../Open2Cards"



func _enter_state():
	reset_round()
	var cards := []
	cards = create_cards()
	#print("Cards: ", cards)
	#print("Number of Cards: ", len(cards))
	game.closed_pile.add_cards(cards, false)
	game.closed_pile.shuffle()
	for i in range(12):
		await game.closed_pile.move_top_card_to_pile(game.player_1.grid.get_pile(i))
	for i in range(12):
		await game.closed_pile.move_top_card_to_pile(game.player_2.grid.get_pile(i))
	await game.closed_pile.move_top_card_to_pile(game.open_pile)
	await game.open_pile.get_top_card().turn_card()
	get_parent().next(open_2_cards)
	game.arrow.show()


func reset_round() -> void:
	game.player_1.reset_round()
	game.player_2.reset_round()
	
	game.closed_pile.clear()
	game.open_pile.clear()
	game.active_card.clear()
	

func create_cards() -> Array[Card]:
	var cards : Array[Card] = []
	for i in range(15):
		var card = CardScene.instantiate()
		card.value = 0
		cards.append(card)
	for i in range(5):
		var card = CardScene.instantiate()
		card.value = -2
		cards.append(card)
	for i in range(10):
		var card = CardScene.instantiate()
		card.value = -1
		cards.append(card)
	for val in range(1, 13):
		for i in range(10):
			var card = CardScene.instantiate()
			card.value = val
			cards.append(card)
	return cards
