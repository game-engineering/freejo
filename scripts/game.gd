extends Node2D

class_name Game

@onready var closed_pile : Pile = $ClosedPile
@onready var open_pile : Pile = $OpenPile
@onready var player_1 : Player = $Player1
@onready var player_2 : Player = $Player2
@onready var active_card : Pile = $ActiveCard

@onready var current_player : Player = player_1
@onready var game_states = $GameStates
@onready var start_state = $GameStates/SetupRound

@onready var arrow : Sprite2D= $Arrow

func get_other_player() -> Player:
	if current_player == player_1:
		return player_2
	else:
		return player_1


# Called when the node enters the scene tree for the first time.
func _ready():
	game_states.next(start_state)

