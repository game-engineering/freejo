extends Node2D

class_name Card

@onready var label := $Front/NameLabel
@onready var front := $Front
@onready var back := $Back



var face_visible := true:
	set(value):
		face_visible = value			
		if face_visible:
			back.hide()
			front.show()
		else:
			front.hide()
			back.show()


func turn_card():
	var tween = create_tween()
	var default_transform = transform
	var _scale_x_centered = func (scale_value):
		transform = default_transform.translated(Vector2(-40, -60)).scaled(Vector2(scale_value, 1)).translated(Vector2(40, 60))
	tween.tween_method(_scale_x_centered, 1.0, 0.0, 0.1)
	tween.tween_property(self, "face_visible", not face_visible, 0)
	tween.tween_method(_scale_x_centered, 0.0, 1.0, 0.1)
	await tween.finished

@export var value : int = 0:
	set(newval):
		value = newval
		if label:
			label.text = str(newval)


func _ready():
	#print("Card is ready: ", self)
	label.text = str(value)


		


func _to_string():
	return str(value)
